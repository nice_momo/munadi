import QtQuick 2.5
import QtQuick.Window 2.0
import Qt.labs.settings 1.0
import QtQuick.Controls 2.0
import QtPositioning 5.5
import QtLocation 5.6
import QtMultimedia 5.5

import "qrc:/data/Adhan.js" as Adhan

ApplicationWindow {
    id: mainWindow

    width: 400
    height: 600
    LayoutMirroring.childrenInherit: true
    LayoutMirroring.enabled: Qt.application.layoutDirection === Qt.RightToLeft
                             || qsTr("QT_LAYOUT_DIRECTION",
                                     "QGuiApplication") === "RTL"

    Component.onCompleted: {
        if (engine.isStartupFlag()) {
            showMinimized()
        } else {
            show()
        }

        //        if(mm.checkForUpdates) {
        //            updater.check()
        //        }

        // Only display what's new once
        if (news.newsVersion !== engine.getVersionNo()) {
            whatsNewPopup.text = engine.getWhatsNew()
            whatsNewPopup.open()
            news.newsVersion = engine.getVersionNo()
        }

        console.debug('UI Dir: ', Qt.application.layoutDirection)
    }

    Component.onDestruction: {
        mm.prayerConfig = mm.prayerConfig
    }

    Theme {
        id: theme
    }

    // Globals
    MunadiModel {
        id: mm
    }

    MediaPlayer {
        id: athan
        property bool isOn: playbackState === MediaPlayer.PlayingState
    }

    Timer {
        interval: 1000
        running: true
        repeat: true

        onTriggered: {
            mm.currentDate = new Date()

            if (mm.currentDate.getSeconds() === 0) {

                if (mm.currentPrayer !== Adhan.adhan.Prayer.None
                        && mm.currentPrayer !== Adhan.adhan.Prayer.Sunrise
                        && mm.timeForCurrentPrayer.getHours(
                            ) === mm.currentDate.getHours()
                        && mm.timeForCurrentPrayer.getMinutes(
                            ) === mm.currentDate.getMinutes()) {

                    athan.volume = mm.prayerConfig[mm.currentPrayer].volume
                    athan.play()

                    if (mm.showOnAthan) {
                        mainWindow.show()
                        mainWindow.raise()
                        mainWindow.requestActivate()
                    }
                }
            }

            if (athan.isOn) {
                mm.timeLeftStr = qsTr("%1 Athan ...").arg(
                            mm.prayerNames[mm.currentPrayer])
            } else {

                if (mm.currentPrayer === Adhan.adhan.Prayer.None) {
                    // Yesterday's Isha!
                    var yesterday = new Date()
                    yesterday.setDate(yesterday.getDate() - 1)
                    console.debug(yesterday)
                    mm.timeForCurrentPrayer
                            = (new Adhan.adhan.PrayerTimes(mm.coordinates,
                                                           yesterday,
                                                           mm.params)).timeForPrayer(
                                Adhan.adhan.Prayer.Isha)
                } else if (mm.nextPrayer === Adhan.adhan.Prayer.None) {
                    // Next day's Fajr!
                    var tomorrow = new Date()
                    tomorrow.setDate(tomorrow.getDate() + 1)
                    mm.timeForNextPrayer = (new Adhan.adhan.PrayerTimes(mm.coordinates,
                                                                        tomorrow,
                                                                        mm.params)).timeForPrayer(
                                Adhan.adhan.Prayer.Fajr)
                }

                var totalMins = (mm.timeForNextPrayer.getTime(
                                     ) - mm.currentDate.getTime()) / 60000
                var hrs = Math.floor(totalMins / 60)
                var mins = Math.floor(totalMins % 60)

                mm.timeLeftStr = hrs + qsTr("h ") + mins + qsTr(
                            "m until ") + (mm.prayerNames[mm.nextPrayer]
                                           || qsTr("Fajr"))
            }
        }
    }

    // Below used for variables worth remembering only
    Settings {
        property alias cityName: mm.cityName
        property alias countryName: mm.countryName
        property alias timeZone: mm.timeZone
        property alias latitude: mm.latitude
        property alias longitude: mm.longitude
        property alias calcMethodIndex: mm.calcMethodIndex
        property alias madhab: mm.madhab
        property alias overrideOffsetBy: mm.overrideOffsetBy
        property alias prayerConfig: mm.prayerConfig
        property alias audioList: mm.audioList
        property alias audioListIndex: mm.audioListIndex
    }

    LocationService {
        id: ls
    }

    // Window content
    Settings {
        category: "Window"
        property alias width: mainWindow.width
        property alias height: mainWindow.height
        property alias x: mainWindow.x
        property alias y: mainWindow.y
    }

    SwipeView {
        id: pagesHolder

        anchors.fill: parent
        focus: false
        currentIndex: tabBar.currentIndex

        MainPage {
            id: mainPage
        }

        SettingsPage {
            id: settingsPage
        }
    }

    footer: TabBar {
        visible: true
        id: tabBar

        //anchors.topMargin: 20
        y: 20
        focus: false
        position: TabBar.Footer
        width: parent.width
        currentIndex: pagesHolder.currentIndex

        TabButton {
            text: qsTr("Home")
        }
        TabButton {
            text: qsTr("Settings")
        }
    }

    Popup {
        id: whatsNewPopup

        parent: ApplicationWindow.overlay
        modal: true

        property alias text: textArea.text

        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        width: parent.width * 0.8
        height: parent.height * 0.8

        Flickable {
            id: flickable
            anchors.fill: parent

            TextArea.flickable: TextArea {
                id: textArea
                readOnly: true
                wrapMode: TextArea.Wrap
                textFormat: TextArea.RichText
            }

            ScrollBar.vertical: ScrollBar {}
        }
    }

    Settings {
        id: news
        category: 'App'
        property string newsVersion
    }

    //    Connections {
    //        target: updater
    //        onUpdateAvailable: {
    //            whatsNewPopup.text = info
    //            whatsNewPopup.open()
    //        }
    //    }
}
